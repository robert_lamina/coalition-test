import ProductMain from './components/ProductMain'
import ProductList from './components/ProductList'
import NewProduct from './components/NewProduct'
import Product from './components/Product'

export const routes = [
	{
		path: '/',
		component: ProductMain,
		children: [
			{
				path: '/',
				component: ProductList
			},
			{
				path: ':id',
				component: Product
			},
			{
				path: 'product/new',
				component: NewProduct
			}
		]
	}
]