require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import Moment from 'vue-moment';
import {routes} from './routes';
import StoreData from './store';
import App from './App.vue';
import {initialize} from './helpers/general';

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(Moment);
Vue.use(Vuetify, {
	 theme: {
	    "primary": "#1F2333",
	    "secondary": "#323755",
	    "accent": "#1F2333",
	    "error": "#FF5252",
	    "info": "#2196F3",
	    "success": "#4CAF50",
	    "warning": "#FFC107"
	  }
});

const store = new Vuex.Store(StoreData);

const router = new VueRouter({
	routes,
	mode: 'history'
});

initialize(store, router);

const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
