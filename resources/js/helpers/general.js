export function initialize(store, router) {
	axios.interceptors.request.use( (config) => {
		store.commit('loader', true);

		return config;
	}, (error) => {
		store.commit('loader', false);

		return Promise.reject(error);
	});

	axios.interceptors.response.use( (response) => {
		store.commit('loader', false);

		return response;
	}, (error) => {

		store.commit('loader', false);
		return Promise.reject(error);
	});
}

export function setAuthorization(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
}