export default {
	state: {
		loading: false,
		products: []
	},
	getters: {
		isLoading(state) {
			return state.loading;
		},
		products(state) {
            return state.products;
        }
	},
	mutations: {
		loader(state, payload) {
			state.loading = payload;
		},
        updateProducts(state, payload) {
            state.products = payload;
        },
	},
	actions: {
		getProducts(context) {
            axios.get('/api/products')
            .then((response) => {
                context.commit('updateProducts', response.data.products);
            })
        }
	}
}