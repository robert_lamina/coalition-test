<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductsController extends Controller
{
    public function all()
    {
        $products = Product::all();
        
        return response()->json([
            "products" => $products
        ], 200);
    }

     public function get($id)
    {
        $product = Product::whereId($id)->first();
        return response()->json([
            "product" => $product
        ], 200);
    }

     public function new(Request $request)
    {
        $product = Product::create($request->only(["product_name", "product_quantity", "product_price"]));
        return response()->json([
            "product" => $product
        ], 200);
    }
}
