<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'product_name' => $faker->name,
        'product_quantity' => str_random(2),
        'product_price' => str_random(3)
    ];
});
